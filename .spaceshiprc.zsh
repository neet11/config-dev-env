# Display time
SPACESHIP_TIME_SHOW=true
SPACESHIP_TIME_FORMAT='%D{%Y/%m/%d %H:%M:%S}'
SPACESHIP_TIME_12HR=false

# Display username always
SPACESHIP_USER_SHOW=always

# Do not truncate path in repos
SPACESHIP_DIR_TRUNC_REPO=false


# Set golang env
SPACESHIP_GOLANG_SHOW=true
SPACESHIP_GOLANG_ASYNC=false

# Set python env
SPACESHIP_PYTHON_SHOW=true
SPACESHIP_PYTHON_ASYNC=false
SPACESHIP_VENV_SHOW=true
SPACESHIP_VENV_ASYNC=false

# Set nodejs env
SPACESHIP_NODE_SHOW=true
SPACESHIP_NODE_ASYNC=false

# Set docker env
SPACESHIP_DOCKER_SHOW=true
SPACESHIP_DOCKER_ASYNC=false

# Set k8s env
SPACESHIP_KUBECTL_SHOW=true
SPACESHIP_KUBECTL_ASYNC=false

# This sets host to be always displayed
SPACESHIP_HOST_SHOW=always

